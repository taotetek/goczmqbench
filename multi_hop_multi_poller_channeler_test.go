package goczmqbench

import (
	"fmt"
	"runtime"
	"sync"
	"testing"
)
import "github.com/zeromq/goczmq"

func benchmarkMultiHopMultiPollerChanneler(size int, maxprocs int, b *testing.B) {
	runtime.GOMAXPROCS(maxprocs)

	fromSock := goczmq.NewSock(goczmq.Pull)
	defer fromSock.Destroy()

	_, err := fromSock.Bind("inproc://benchOutSock")
	if err != nil {
		panic(err)
	}

	toSock := goczmq.NewSock(goczmq.Push)
	defer toSock.Destroy()

	_, err = toSock.Bind("inproc://benchInSock")
	if err != nil {
		panic(err)
	}

	toCommand := goczmq.NewSock(goczmq.Push)
	defer toCommand.Destroy()

	_, err = toCommand.Bind("inproc://benchCommand")
	if err != nil {
		panic(err)
	}

	go func() {
		outSock := goczmq.NewSock(goczmq.Push)
		defer outSock.Destroy()

		err := outSock.Connect("inproc://benchOutSock")
		if err != nil {
			panic(err)
		}

		inSock := goczmq.NewSock(goczmq.Pull)
		defer outSock.Destroy()

		err = inSock.Connect("inproc://benchInSock")
		if err != nil {
			panic(err)
		}

		commandSock := goczmq.NewSock(goczmq.Pull)
		defer commandSock.Destroy()

		err = commandSock.Connect("inproc://benchCommand")
		if err != nil {
			panic(err)
		}

		poller, err := goczmq.NewPoller(inSock, commandSock)
		if err != nil {
			panic(err)
		}

		for i := 0; i < b.N; i++ {
			s := poller.Wait(-1)

			switch s {
			case inSock:
				msg, err := s.RecvMessage()
				if err != nil {
					panic(fmt.Sprintf("s.RecvMsg(): %s", err))
				}

				err = outSock.SendMessage(msg)
				if err != nil {
					panic(err)
				}
			case commandSock:
				command, err := s.RecvMessage()
				if err != nil {
					panic(err)
				}
				if string(command[0]) != "a command" {
					panic("command message is wrong")
				}
			}
		}
	}()

	wg := &sync.WaitGroup{}
	wg.Add(1)

	msgQueue := make(chan [][]byte)

	go func() {

		for msg := range msgQueue {
			err = toSock.SendMessage(msg)
			if err != nil {
				panic(fmt.Sprintf("toSock.SendFrame(payload, FlagNone): %s", err))
			}

			reply, _, err := fromSock.RecvFrame()
			if err != nil {
				panic(fmt.Sprintf("fromSock.RecvFrame(): %s", err))
			}
			if len(reply) != size {
				panic("msg too small")
			}
		}
		wg.Done()
	}()

	payload := make([]byte, size)
	for i := 0; i < b.N; i++ {
		msgQueue <- [][]byte{payload}
	}

	close(msgQueue)
	wg.Wait()
}

func BenchmarkMultiHopMultiPollerChanneler1k1Routine(b *testing.B) {
	benchmarkMultiHopMultiPollerChanneler(1024, 1, b)
}

func BenchmarkMultiHopMultiPollerChanneler4k1Routine(b *testing.B) {
	benchmarkMultiHopMultiPollerChanneler(4096, 1, b)
}

func BenchmarkMultiHopMultiPollerChanneler16k1Routine(b *testing.B) {
	benchmarkMultiHopMultiPollerChanneler(16386, 1, b)
}

func BenchmarkMultiHopMultiPollerChanneler1k2Routines(b *testing.B) {
	benchmarkMultiHopMultiPollerChanneler(1024, 2, b)
}

func BenchmarkMultiHopMultiPollerChanneler4k2Routines(b *testing.B) {
	benchmarkMultiHopMultiPollerChanneler(4096, 2, b)
}

func BenchmarkMultiHopMultiPollerChanneler16k2Routines(b *testing.B) {
	benchmarkMultiHopMultiPollerChanneler(16386, 2, b)
}
