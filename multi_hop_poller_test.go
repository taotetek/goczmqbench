package goczmqbench

import (
	"fmt"
	"testing"
)
import "github.com/zeromq/goczmq"

func benchmarkMultiHopPoller(size int, b *testing.B) {
	fromSock := goczmq.NewSock(goczmq.Pull)
	defer fromSock.Destroy()

	_, err := fromSock.Bind("inproc://benchOutSock")
	if err != nil {
		panic(err)
	}

	toSock := goczmq.NewSock(goczmq.Push)
	defer toSock.Destroy()

	_, err = toSock.Bind("inproc://benchInSock")
	if err != nil {
		panic(err)
	}

	go func() {
		outSock := goczmq.NewSock(goczmq.Push)
		defer outSock.Destroy()

		err := outSock.Connect("inproc://benchOutSock")
		if err != nil {
			panic(err)
		}

		inSock := goczmq.NewSock(goczmq.Pull)
		defer outSock.Destroy()

		err = inSock.Connect("inproc://benchInSock")
		if err != nil {
			panic(err)
		}

		poller, err := goczmq.NewPoller(inSock)
		if err != nil {
			panic(err)
		}

		for i := 0; i < b.N; i++ {
			s := poller.Wait(-1)

			msg, err := s.RecvMessage()
			if err != nil {
				panic(fmt.Sprintf("s.RecvMsg(): %s", err))
			}

			err = outSock.SendMessage(msg)
			if err != nil {
				panic(err)
			}
		}
	}()

	payload := make([]byte, size)
	for i := 0; i < b.N; i++ {
		err = toSock.SendFrame(payload, goczmq.FlagNone)
		if err != nil {
			panic(fmt.Sprintf("toSock.SendFrame(payload, FlagNone): %s", err))
		}

		msg, _, err := fromSock.RecvFrame()
		if err != nil {
			panic(err)
		}
		if len(msg) != size {
			panic("msg too small")
		}
	}
}

func BenchmarkMultiHopPoller1k(b *testing.B) { benchmarkMultiHopPoller(1024, b) }
func BenchmarkMultiHopPoller4k(b *testing.B) { benchmarkMultiHopPoller(4096, b) }
