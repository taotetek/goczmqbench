package goczmqbench

import "testing"
import "github.com/zeromq/goczmq"

func benchmarkSingleHop(size int, b *testing.B) {
	pullSock := goczmq.NewSock(goczmq.Pull)
	defer pullSock.Destroy()

	_, err := pullSock.Bind("inproc://benchSock")
	if err != nil {
		panic(err)
	}

	go func() {
		pushSock := goczmq.NewSock(goczmq.Push)
		defer pushSock.Destroy()
		err := pushSock.Connect("inproc://benchSock")
		if err != nil {
			panic(err)
		}

		payload := make([]byte, size)
		for i := 0; i < b.N; i++ {
			err = pushSock.SendFrame(payload, goczmq.FlagNone)
			if err != nil {
				panic(err)
			}
		}
	}()

	for i := 0; i < b.N; i++ {
		msg, _, err := pullSock.RecvFrame()
		if err != nil {
			panic(err)
		}
		if len(msg) != size {
			panic("msg too small")
		}
	}
}

func BenchmarkSingleHop1k(b *testing.B) { benchmarkSingleHop(1024, b) }
func BenchmarkSingleHop4k(b *testing.B) { benchmarkSingleHop(4096, b) }
